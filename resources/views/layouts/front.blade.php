<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script>
			window.Laravel = {!! json_encode([
					'csrfToken' => csrf_token(),
			]) !!};
	</script>
    <title>Ad Server Demo</title>
@include('layouts.files')
</head>
<body class="">
@include('parts.header')
<!-- Page container -->
<div class="page-container">
<!-- Page content -->
<div class="page-content">
@include('parts.sidebar')
@yield('content')
@include('parts.footer')

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

